# Making of the random password generator using python....

#Importing the random 
import random

print("\n\t\t\t RANDOM PASSWORD GENERATOR\n");
# The data that is being fed
lower_case = "abcdefghijklmnopqrstuvwxyz";
upper_case = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
numbers = "1234567890";
symbols = "!@#$%^&*/.<>\,?;:";

mixture = lower_case + upper_case + numbers + symbols;
length_for_eight = 8;
length_above_eight = 12;

# Generate random password
# for 8 letter password
password_for_eight = "" . join(random.sample(mixture, length_for_eight));
# for 12 letter password
password_above_eight = "" . join(random.sample(mixture, length_above_eight));

# Printing the password
#print(f"The length of the password is: {length_for_pass}")
print(f"Your generated random password of length: {length_for_eight} is : {password_for_eight}");
print(f"YOur generated random passworf of length: {length_above_eight} is : {password_above_eight}");
